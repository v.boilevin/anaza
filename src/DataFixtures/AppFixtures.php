<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Project;
use App\Entity\Status;
use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        
        for ($u = 0; $u < 10; $u++) {
            $user = new User();
            $hash = $this->encoder->encodePassword($user, "password");
            $user->setFirstName($faker->firstName())
                ->setLastName($faker->lastName())
                ->setEmail($faker->email())
                ->setPassword($hash);

            $manager->persist($user);
            for ($i = 0; $i < mt_rand(5, 20); $i++) {
                $project = new Project();
                $project->setName($faker->company())
                    ->setCreationDate($faker->dateTime())
                    ->setUser($user);
                $manager->persist($project);

                for ($j = 0; $j < mt_rand(15, 30); $j++) {
                    $task = new Task();
                    $status = new Status();
                    $status->setLabel($faker->randomElement((['Reported', 'Denied', 'Accepted', 'In progress', 'Review period', 'To prod'])));
                    $manager->persist($status);
                    $task->setLabel($faker->phoneNumber())
                        ->setCreationDate($faker->dateTime('-6 mounths'))
                        ->setDeadLine($faker->dateTime('-1 mounths'))
                        ->setProject($project)
                        ->setStatus($status)
                        ->setDescription($faker->realText());
                    $manager->persist($task);
                    
                }
            }
        }
        $manager->flush();
    }
}
