<?php

namespace App\Events;

use App\Entity\Project;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class ProjectUserSubscriber implements EventSubscriberInterface {

    /**@var  Security*/
    private $security;

    public function __construct(Security $security){
        $this->security = $security;
    }
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUserForProject', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setUserForProject(ViewEvent $event){
        $result = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($result instanceof Project && $method === "POST"){
            $user = $this->security->getUser();
            $result->setUser($user);
        }
    }
}