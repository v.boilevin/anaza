<?php

namespace App\Events;

use App\Entity\Task;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Status;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Repository\ProjectRepository;

class TaskCreationDateSubscriber implements EventSubscriberInterface
{
    /**@var ProjectRepository */
    private $projectRepository;

    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setTaskBeforeStoring', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setTaskBeforeStoring(ViewEvent $event)
    {
        $result = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if ($result instanceof Task && $method === "POST") {
            $today = new \DateTime('NOW');
            $tomorow = new \DateTime('NOW');
            $result->setCreationDate($today);
            $result->setProject($this->projectRepository->getProjectByName($result->getProject()->getName())[0]);
            if ($result->getDeadLine() == null) {
                $result->setDeadLine($tomorow->modify('+7 day'));
            }
        }
    }
}
