<?php

namespace App\Entity;

use App\Controller\UpdateStatus;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\TaskRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 * @ApiResource(
 *  subresourceOperations={
 *      "api_projects_tasks_get_subresource"={
 *          "normalization_context"={"groups"={"tasks_subresource"}}
 *      }
 *  },
 *  normalizationContext={"groups"={"tasks_read"}},
 *  denormalizationContext={"groups"={"tasks_write"}},
 * )
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"tasks_read", "projects_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"tasks_read", "projects_read", "tasks_write"})
     * @Assert\NotBlank(message="Une tache doit avoir un libéllé")
     * @Assert\Type(type="string", message="Mauvais format d'entré")
     */
    private $label;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"tasks_read", "tasks_write"})
     * @Assert\NotBlank(message="La date de création doit etre renseignée")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"tasks_read", "tasks_write"})
     * @Assert\NotBlank(message="Une tache doit avoir une dead line")
     */
    private $deadLine;

    /**
     * @ORM\Column(type="text")
     * @Groups({"tasks_read", "projects_read", "tasks_subresource", "tasks_write"})
     * @Assert\NotBlank(message="A task must be described")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="tasks", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"tasks_read", "tasks_write"})
     * @Assert\NotBlank(message="Le project doit etre renseigné")
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity=Status::class, inversedBy="tasks", cascade={"persist"})
     * @Groups({"tasks_read", "projects_read", "tasks_write"})
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getDeadLine(): ?\DateTimeInterface
    {
        return $this->deadLine;
    }

    public function setDeadLine(\DateTimeInterface $deadLine): self
    {
        $this->deadLine = $deadLine;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }
}
