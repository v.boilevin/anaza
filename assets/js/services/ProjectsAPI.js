import Axios from "axios";

function findAll() {
  return Axios.get("http://127.0.0.1:8000/api/projects").then(
    (response) => response.data["hydra:member"]
  );
}

function deleteProject(id) {
  return Axios.delete("http://127.0.0.1:8000/api/projects/" + id);
}

export default {
  findAll,
  delete: deleteProject,
};
