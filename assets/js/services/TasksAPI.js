import Axios from "axios";

function findAll() {
  return Axios.get("http://127.0.0.1:8000/api/tasks").then(
    (response) => response.data["hydra:member"]
  );
}

function find(id) {
  return Axios.get("http://127.0.0.1:8000/api/tasks/" + id).then(
    (response) => response.data
  );
}

function deleteTask(id) {
  return Axios.delete("http://127.0.0.1:8000/api/tasks/" + id).then(
    (response) => response.data
  );
}

function newTask(task) {
  return Axios.post("http://127.0.0.1:8000/api/tasks", task).then(
    (response) => response.data
  );
}

function updateTask(task, param) {
  Axios.defaults.headers["Content-Type"] = "application/merge-patch+json";
  return Axios.patch("http://127.0.0.1:8000/api/tasks/" + param, task)
    .then((response) => response.data)
    .then(() => (Axios.defaults.headers["Content-Type"] = "application/json"));
}

export default {
  findAll,
  find,
  delete: deleteTask,
  new: newTask,
  update: updateTask,
};
