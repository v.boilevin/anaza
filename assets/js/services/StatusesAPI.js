import Axios from "axios";

function findAll() {
  return Axios.get("http://127.0.0.1:8000/api/statuses").then(
    (response) => response.data["hydra:member"]
  );
}

function deleteStatus(id) {
  return Axios.delete("http://127.0.0.1:8000/api/statuses/" + id);
}

function getList() {
  return Axios.get("http://127.0.0.1:8000/api/statuses_lists").then(
    (response) => response.data["hydra:member"]
  );
}

export default {
  findAll,
  delete: deleteStatus,
  list: getList,
};
