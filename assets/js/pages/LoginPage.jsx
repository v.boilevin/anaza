import React, { useContext, useState } from "react";
import AuthAPI from "../services/AuthAPI";
import AuthContext from "../contexts/AuthContext";
import Field from "../components/forms/Field";

const LoginPage = ({ history }) => {
  const { setIsAuthenticated } = useContext(AuthContext);
  const [credentials, setCredentials] = useState({
    username: "",
    password: "",
  });

  const [error, setError] = useState("");

  const handleChange = ({ currentTarget }) => {
    const { value, name } = currentTarget;
    setCredentials({ ...credentials, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      await AuthAPI.authenticate(credentials);
      setError("");
      setIsAuthenticated(true);
      history.replace("/");
    } catch (error) {
      console.log(error.response);
      setError("No accounts associated to this @email");
    }
  };

  return (
    <>
      <h1>LOGIN PAGE</h1>
      <form onSubmit={handleSubmit}>
        <Field
          label="@Email"
          name="username"
          type="email"
          value={credentials.username}
          onChange={handleChange}
          placeholder="@email"
          error={error}
        />
        <Field
          label="Password"
          type="password"
          name="password"
          value={credentials.password}
          onChange={handleChange}
          placeholder="Password..."
          error={error}
        />
        <div classnme="form-group">
          <button type="submit" className="btn btn-success">
            GO
          </button>
        </div>
      </form>
    </>
  );
};

export default LoginPage;
