import React, { useState } from "react";
import { Link } from "react-router-dom";
import Field from "../components/forms/Field";
import Axios from "axios";

const ProjectPage = (props) => {
  const [project, setProject] = useState({
    name: ""
  });

  const [errors, setErrors] = useState({
    name: ""
  });

  const handleChange = ({ currentTarget }) => {
    const { name, value } = currentTarget;
    setProject({ ...project, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await Axios.post(
        "http://127.0.0.1:8000/api/projects",
        project
      );
      console.log(response.data);
    } catch (error) {
      if(error.response.data.violations){
          const apiErrors = {};
          error.response.data.violations.map(violation => {
            apiErrors[violation.propertyPath] = violation.message;
          });
          setErrors(apiErrors);
      }
    }
  };

  return (
    <>
      <h1>Create a project</h1>
      <form onSubmit={handleSubmit}>
        <Field
          name="name"
          label="Name"
          value={project.name}
          onChange={handleChange}
          error={errors.name}
        />
        <div className="form-group">
          <button type="submit" className="btn btn-success">
            Regrister
          </button>
          <Link to="/projects" className="btn btn-link">
            Back to grid
          </Link>
        </div>
      </form>
    </>
  );
};

export default ProjectPage;
