import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import Field from "../components/forms/Field";
import moment from "moment";
import ProjectsAPI from "../services/ProjectsAPI";
import StatusesAPI from "../services/StatusesAPI";
import TasksAPI from "../services/TasksAPI";

const TaskPage = ({ history }) => {
  const [projects, setProjects] = useState([]);
  const [param, setParam] = useState();
  const [statuses, setStatuses] = useState([]);
  const { id } = useParams();
  const [task, setTask] = useState({
    label: "",
    description: "",
    status: {
      label: "",
    },
  });
  const [newTask, setNewTask] = useState({
    label: "",
    description: "",
    creationDate: moment().format(),
    status: {
      label: "",
    },
    project: {
      name: "",
    },
  });

  const fetchProjects = async () => {
    try {
      const data = await ProjectsAPI.findAll();
      setProjects(data);
    } catch (error) {
      console.log(error.response);
    }
  };

  const getParams = () => {
    try {
      setParam(id);
    } catch (error) {
      console.log(error.response);
    }
  };

  useEffect(() => {
    getParams();
    fetchProjects();
    fetchTask();
    fetchStatuses();
  }, []);

  const fetchTask = async () => {
    try {
      if (id != "new") {
        const data = await TasksAPI.find(id);
        setTask(data);
      }
    } catch (error) {
      console.log(error.response);
    }
  };

  const fetchStatuses = async () => {
    try {
      const data = await StatusesAPI.list();
      setStatuses(data);
    } catch (error) {
      console.log(error.response);
    }
  };

  const [errors, setErrors] = useState({
    label: "",
    status: "",
    description: "",
    project: {},
  });

  const handleChange = ({ currentTarget }) => {
    const { name, value } = currentTarget;
    if (id != "new") {
      if ([name] == "status.label") {
        setTask({ ...task, status: { label: value } });
      } else {
        setTask({ ...task, [name]: value });
      }
    } else {
      if ([name] == "status.label") {
        setNewTask({ ...newTask, status: { label: value } });
      } else if ([name] == "project.name") {
        setNewTask({ ...newTask, project: { name: value } });
      } else {
        setNewTask({ ...newTask, [name]: value });
      }
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (param == "new") {
        const response = await TasksAPI.new(newTask);
      } else {
        const response = await TasksAPI.update(task, param);
      }
      history.replace("/");
    } catch (error) {
      if (error.response.data.violations) {
        const apiErrors = {};
        error.response.data.violations.map((violation) => {
          apiErrors[violation.propertyPath] = violation.message;
        });
        setErrors(apiErrors);
      }
    }
  };

  return (
    <div>
      {(param == "new" && (
        <>
          <h1>New Task</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <label htmlFor="exampleSelect1" className="form-label mt-4">
                Project
              </label>
              <select
                className="form-select"
                id="project"
                data-dashlane-rid="3388ccd0625a05d3"
                data-form-type="other"
                onChange={handleChange}
                name="project.name"
              >
                {projects.map((project) => (
                  <option key={project.id} value={project.name}>
                    {project.name}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="exampleSelect1" className="form-label mt-4">
                Status
              </label>
              <select
                className="form-select"
                id="status"
                data-dashlane-rid="3388ccd0625a05d3"
                data-form-type="other"
                onChange={handleChange}
                name="status.label"
              >
                {statuses.map((status) => (
                  <option key={status.id} value={status.label}>
                    {status.label}
                  </option>
                ))}
              </select>
            </div>
            <Field
              name="label"
              placeholder="label"
              value={newTask.label}
              onChange={handleChange}
              error={errors.label}
            />
            <Field
              name="description"
              placeholder="description"
              value={newTask.description}
              onChange={handleChange}
              error={errors.description}
            />
            <div className="form-group mt-3">
              <button type="submit" className="btn btn-success me-3">
                Save
              </button>
              <Link to="/" className="btn btn-link">
                Back to grid
              </Link>
            </div>
          </form>
        </>
      )) || (
        <>
          {" "}
          <h1>Edit Task</h1>
          <form onSubmit={handleSubmit}>
            <Field
              name="label"
              placeholder="label"
              value={task.label}
              onChange={handleChange}
              error={errors.label}
            />
            <div className="form-group">
              <label htmlFor="status" className="form-label mt-4">
                Status
              </label>
              <select
                className="form-select"
                id="status"
                data-dashlane-rid="3388ccd0625a05d3"
                data-form-type="other"
                onChange={handleChange}
                value={task.status.label}
                name="status.label"
              >
                {statuses.map((status) => {
                  if (status.label == task.status.label) {
                    return (
                      <option key={status.id} value={task.status.label}>
                        {status.label}
                      </option>
                    );
                  } else {
                    return (
                      <option key={status.id} value={status.label}>
                        {status.label}
                      </option>
                    );
                  }
                })}
              </select>
            </div>
            <Field
              name="description"
              placeholder="description"
              value={task.description}
              onChange={handleChange}
              error={errors.description}
            />
            <div className="form-group mt-3">
              <button type="submit" className="btn btn-success me-3">
                Save
              </button>
              <Link to="/" className="btn btn-link">
                Back to grid
              </Link>
            </div>
          </form>
        </>
      )}
    </div>
  );
};

export default TaskPage;
