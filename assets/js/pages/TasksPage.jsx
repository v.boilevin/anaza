import moment from "moment";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Pagination from "../components/Pagination";
import TasksAPI from "../services/TasksAPI";

const TasksPage = (props) => {
  const [tasks, setTasks] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState("");
  const itemsPerPage = 10;

  const formatDate = (str) => moment(str).format("DD/MM/YYYY");

  const fetchTasks = async () => {
    try {
      const data = await TasksAPI.findAll();
      setTasks(data);
    } catch (error) {
      console.log(error.response);
    }
  };

  useEffect(() => {
    fetchTasks();
  }, []);

  const handleChangePage = (page) => {
    setCurrentPage(page);
  };

  const handleSearch = ({ currentTarget }) => {
    setSearch(currentTarget.value);
    setCurrentPage(1);
  };

  const handleDelete = async (id) => {
    const originalTasks = [...tasks];
    setTasks(tasks.filter((task) => task.id !== id));
    try {
      await TasksAPI.delete(id);
    } catch (error) {
      console.log(error.response);
      setTasks(originalTasks);
    }
  };

  const filteredTasks = tasks.filter((i) =>
    i.label.toLowerCase().includes(search.toLowerCase())
  );

  const paginatedTasks = Pagination.getData(
    filteredTasks,
    currentPage,
    itemsPerPage
  );

  return (
    <>
      <div className="mb-2 d-flex justify-content-between align-items-center">
        <h1>Tasks List</h1>
        <Link className="btn btn-primary" to="/tasks/new">
          create a task
        </Link>
      </div>
      <div className="form-group">
        <input
          type="text"
          onChange={handleSearch}
          value={search}
          className="form-control"
          placeholder="Search an task..."
        />
      </div>
      <table className="table table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Label</th>
            <th>Project</th>
            <th>Status</th>
            <th>Created At</th>
            <th>Deadline</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {paginatedTasks.map((task) => (
            <tr key={task.id}>
              <td>{task.id}</td>
              <td>
                <Link className="" to={`/tasks/${task.id}`}>
                  {task.label}
                </Link>
              </td>
              <td>{task.project.name}</td>
              <td>
                <span className="">{task.status.label}</span>
              </td>

              <td>{formatDate(task.creationDate)}</td>
              <td>{formatDate(task.deadLine)}</td>
              <td>
                <button
                  className="btn btn-sm btn-danger"
                  onClick={() => handleDelete(task.id)}
                >
                  Supprimer
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Pagination
        currentPage={currentPage}
        itemsPerPage={itemsPerPage}
        onPageChanged={handleChangePage}
        length={filteredTasks.length}
      />
    </>
  );
};

export default TasksPage;
