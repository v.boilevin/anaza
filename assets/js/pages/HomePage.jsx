import React, { useEffect, useState, useContext } from "react";
import { Button, Collapse } from "react-bootstrap";
import { Link } from "react-router-dom";
import ProjectsAPI from "../services/ProjectsAPI";
import StatusesAPI from "../services/StatusesAPI";
import { DragDropContext } from "react-beautiful-dnd";
import AuthContext from "../contexts/AuthContext";

const HomePage = (props) => {
  const [projects, setProjects] = useState([]);
  const [statuses, setStatuses] = useState([]);
  const [taskList, setTasks] = useState([]);
  const { isAuthenticated } = useContext(AuthContext);

  const fetchProjects = async () => {
    try {
      const data = await ProjectsAPI.findAll();
      setProjects(data);
    } catch (error) {
      console.log(error.response);
    }
  };

  const fetchStatuses = async () => {
    try {
      const data = await StatusesAPI.list();
      setStatuses(data);
    } catch (error) {
      console.log(error.response);
    }
  };

  useEffect(() => {
    fetchProjects();
    fetchStatuses();
  }, []);

  return isAuthenticated ? (
    <>
      <h1>Current Projects</h1>
      <br />
      {projects.map((project) => (
        <div
          key={project.id}
          className="row"
          style={{ marginLeft: -17 + "rem" }}
        >
          <h2>{project.name}</h2>
          <div className="d-flex">
            {statuses.map((status) => (
              <div
                key={status.id}
                className="card border-primary mb-3"
                style={{ maxWidth: 19 + "rem", minWidth: 19 + "rem" }}
              >
                <div className="card-header">{status.label}</div>
                <div className="card-body">
                  {project.tasks
                    .filter((task) => task.status.label == status.label)
                    .map((task) => (
                      <div
                        key={task.id}
                        className="card border-primary mb-3"
                        style={{ maxWidth: 18 + "rem" }}
                      >
                        <div className="card-header">
                          <Link className="" to={`/tasks/${task.id}`}>
                            <p className="card-text">{task.label}</p>
                          </Link>
                        </div>
                        <div className="card-body">
                          <p className="card-text">{task.description}</p>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            ))}
          </div>
        </div>
      ))}
    </>
  ) : (
    <div>Bienvenu chez ANAZA</div>
  );
};

export default HomePage;
