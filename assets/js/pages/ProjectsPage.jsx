import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Pagination from "../components/Pagination";
import ProjectsAPI from "../services/ProjectsAPI";

const ProjectsPage = (props) => {
  const [projects, setProjects] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState("");
  const itemsPerPage = 10;

  // recup les projects
  const fetchProjects = async () => {
    try {
      const data = await ProjectsAPI.findAll();
      setProjects(data);
    } catch (error) {
      console.log(error.response);
    }
  };

  // au chargement du composant on fetch les projects
  useEffect(() => {
    fetchProjects();
  }, []);

  const handleDelete = async (id) => {
    const originalProjects = [...projects];
    setProjects(projects.filter((project) => project.id !== id));
    try {
      await ProjectsAPI.delete(id);
    } catch (error) {
      setProjects(originalProjects);
      console.log(error.response);
    }
  };

  const handleChangePage = (page) => {
    setCurrentPage(page);
  };

  const handleSearch = ({ currentTarget }) => {
    setSearch(currentTarget.value);
    setCurrentPage(1);
  };

  const filteredProjects = projects.filter((c) =>
      c.name.toLowerCase().includes(search.toLowerCase())
  );
  
  const paginatedProjects = Pagination.getData(
    filteredProjects,
    currentPage,
    itemsPerPage
  );

  return (
    <>
      <div className="mb-2 d-flex justify-content-between align-items-center">
        <h1>Projects List</h1>
        <Link className="btn btn-primary" to="/projects/new">
          create a project
        </Link>
      </div>
      <div className="form-group">
        <input
          type="text"
          onChange={handleSearch}
          value={search}
          className="form-control"
          placeholder="Search a project..."
        />
      </div>
      <table className="table table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Nombre de tache en cours</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {paginatedProjects.map((project) => (
            <tr key={project.id}>
              <td>{project.id}</td>
              <td>
                <a href="#">
                  {project.name}
                </a>
              </td>
              <td>{project.tasks.length}</td>
              <td>
                <button
                  onClick={() => handleDelete(project.id)}
                  disabled={project.tasks.length > 0}
                  className="btn btn-sm btn-danger"
                >
                  Supprimer
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      {itemsPerPage < filteredProjects.length && (
        <Pagination
          currentPage={currentPage}
          itemsPerPage={itemsPerPage}
          length={projects.length}
          onPageChanged={handleChangePage}
        />
      )}
    </>
  );
};

export default ProjectsPage;
